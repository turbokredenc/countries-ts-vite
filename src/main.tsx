import React from 'react'
import ReactDOM from 'react-dom/client'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import App from './App.tsx'
import '@picocss/pico'
import './index.css'
import { CountryProvider } from './CountryContext.tsx';

const client = new ApolloClient({
  uri: 'https://countries.trevorblades.com/',
  cache: new InMemoryCache(),
});

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <CountryProvider>
        <App />
      </CountryProvider>
    </ApolloProvider>
  </React.StrictMode>,
)
