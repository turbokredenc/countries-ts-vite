import { useQuery } from '@apollo/client';
import { useState } from 'react'
import CountryCard from './../components/CountryCard/CountryCard';
import { GET_CONTINENTS, GET_COUNTRIES_BY_CONTINENT_CODE, GET_COUNTRY_BY_COUNTRY_CODE } from './../queries';
import Navbar from './components/Navbar'

const SelectCountries = () => {

  const [continentCode, setContinentCode] = useState('')
  const [countryCode, setCountryCode] = useState('')
  
  // GET ALL CONTINETS
  const continentsQuery = useQuery(GET_CONTINENTS);
  
  // SELECT SPECIFIC CONTINENT
  const countriesQuery = useQuery(GET_COUNTRIES_BY_CONTINENT_CODE, {
    variables: {code: continentCode }
  });
  
  
  
  if (continentsQuery.loading) return <p>Loading...</p>
  if (continentsQuery.error) return <p>Error : {continentsQuery.error.message}</p>
  
  const hasCountries = !!countriesQuery.data?.continent?.countries
  
  console.log(countryCode,'countryCode')
  
  type ContinentType = {
    code: string | number; // Allow both strings and numbers for the ID
    countries: Country[];
    name: string;
  }
  
  
  const updateCodes = (value: string) => {
      setContinentCode(value)
      setCountryCode('') 
    }

  return (
    <div className="grid">
    <div>
      {/* <SelectBox name="Continents" label="Choose a continent:" handleSelect={(e) => handleContinents(e.target.value)} isDisabled={false} items={continentsQuery.data.continents} /> */}
      {/* <SelectBox name="Countries" label="Choose a country:" handleSelect={(e) => setCountryCode(e.target.value)} isDisabled={!hasCountries} items={countriesQuery.data?.continent?.countries.data.continents} /> */}
        <label htmlFor="continent-select">Choose a continent:</label>
        <select id="continent-select" onChange={e => updateCodes(e.target.value)} name="Continents">
          <option value=''>--CHOOSE OPTION--</option>
          {continentsQuery.data.continents?.map((continent: ContinentType)=> <option key={continent.code} value={continent.code}>{continent.name}</option>)}
        </select>
        
        <label htmlFor='countries-select'>Choose a country:</label>
        <select id='countries-select' onChange={(e) => setCountryCode(e.target.value)} name="Countries" disabled={!hasCountries}>
        <option value=''>--CHOOSE OPTION--</option>
        {hasCountries && countriesQuery.data.continent.countries.map((country: Country)=> <option key={country.code} value={country.code}>{country.name}</option>)}
        </select>
    </div>
    <div>
      {!!countryCode && <CountryCard countryCode={countryCode} />}
    </div>
</div>
  )
}

export default SelectCountries