import React, { useContext } from 'react'
import CountryContext from '../CountryContext'

const MyCountries = () => {
  const { countryArr } = useContext(CountryContext)
  console.log(countryArr,'countryArr')
  return (
    <div>
      <span></span>
      <span>{countryArr}</span>
    </div>
  )
}

export default MyCountries