import { createContext, useState, ReactNode, Dispatch, SetStateAction } from 'react';

type CountryContextType = {
  countryArr: Array<string>;
  setCountry: Dispatch<SetStateAction<Array<string>>>;
};

const CountryContext = createContext<CountryContextType>({
  countryArr: [], // default value
  setCountry: () => {} // default setter function
});

export const CountryProvider = ({ children }: { children: ReactNode }) => {
  const [countryArr, setCountry] = useState<Array<string>>([]);

  return (
    <CountryContext.Provider value={{ countryArr, setCountry }}>
      {children}
    </CountryContext.Provider>
  );
};

export default CountryContext
