import { gql } from "@apollo/client";

export const GET_COUNTRIES_BY_CONTINENT_CODE = gql`
query GetCountriesByContinentCode($code: ID!) {
  continent(code: $code) {
    name
    code
    countries {
        code
        name
        phone
        capital
        currency
        }
    }
}
`