import { gql } from "@apollo/client";

export const GET_COUNTRY_BY_COUNTRY_CODE = gql`
query GetCountry($code: ID!) {
  country(code: $code) {
    code
    name
    awsRegion
    subdivisions {
      code
      name
    }
    native
    capital
    emoji
    currencies
    currency
    phones
    phone
    emojiU
    states {
      code
      name
    }
    languages {
      code
      name
    }
  }
}
`