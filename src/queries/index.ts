import { GET_COUNTRY_BY_COUNTRY_CODE } from './countryByCodeQuery'
import { GET_CONTINENTS } from './allContinetsQuery'
import { GET_COUNTRIES_BY_CONTINENT_CODE } from './countriesByContinentCodeQuery'

export { GET_COUNTRY_BY_COUNTRY_CODE, GET_CONTINENTS, GET_COUNTRIES_BY_CONTINENT_CODE }