
import SelectCountries from './pages/SelectCountries';
import { createRoot } from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import { BrowserRouter, Routes } from "react-router-dom";
import Navbar from './components/Navbar';
import Test from './pages/Test';
import MyCountries from './pages/MyCountries';

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <h1>Hello World</h1>
        <Link to="about">About Us</Link>
      </div>
    ),
  },
  {
    path: "about",
    element: <div>About</div>,
  },
]);



function App() {


  return (
    <BrowserRouter >
      <header>
        <h1>My Super Cool App</h1>
        <Navbar />
      </header>
      <main>
        <Routes>
          <Route path="/" element={<SelectCountries />} /> {/* 👈 Renders at /app/ */}
          <Route path="test1" element={<Test />} />
          <Route path="test2" element={<MyCountries />} />
        </Routes>
      </main>
      <footer>©️ me 2023</footer>
    </BrowserRouter>
  )
}

export default App
