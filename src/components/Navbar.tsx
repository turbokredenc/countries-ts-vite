import {Link} from 'react-router-dom';

const Navbar = () => {
  return (
    <nav>
      <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/test1">Test1</Link></li>
          <li><Link to="/test2">Test2</Link></li>
      </ul>
    </nav>
  )
}

export default Navbar