import { useContext } from 'react';
import CountryContext from '../../CountryContext';
import { useQuery } from '@apollo/client';
import { GET_COUNTRY_BY_COUNTRY_CODE } from './../../queries';

const CountryCard = ({countryCode}) => {
  // SELECT SPECIFIC COUNTRY
  const { countryArr, setCountry } = useContext(CountryContext)
  const countryResult = useQuery(GET_COUNTRY_BY_COUNTRY_CODE, {
    variables: {code: countryCode }
  })

  const addToMyCountries = (code) => {
    if (countryArr.includes(code)) return
    setCountry([...countryArr, code])
  }
  const deleteFromMyCountries = (codeToDelete: string) => {
    const filteredCountries = countryArr.filter((code) => code === codeToDelete)
    setCountry(filteredCountries)
  }



  if (countryResult.loading) return <p>Loading...</p>
  if (countryResult.error) return <p>Error</p>
  const { native, name, code, capital, currency, phone, languages } = countryResult.data.country

  return (
    <article>
        <header>
        <h4>COUNTRY</h4>
        <div><i>{native}</i></div>
        <h5>{name}</h5>
        </header>
        <h5>Description</h5>
            <div>Code: {code}</div>
            <div>Capital: {capital}</div>
            <div>Currency: {currency}</div>
            <div>Phone: {phone}</div>
            <div>Languages:</div>
            <ul>
              {languages.map((language) => <li>{language.name}</li> )}
            </ul>
        <footer>
            <button onClick={()=>addToMyCountries(code)}>Add Country Card</button>
            <button onClick={()=>deleteFromMyCountries(code)}>Delete Card</button>
            <button onClick={()=>setCountry([])}>Delete All Cards</button>
            <p>{countryArr}</p>
        </footer>
  </article>
  )
}

export default CountryCard